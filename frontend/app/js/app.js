"use strict";
angular.module("easyrun", [
  "constants",
  "ui.router",
  "templates",
  "LocalStorageModule",
  "720kb.datepicker"
])
.config( ($stateProvider, $urlRouterProvider, $sceDelegateProvider, SER) => {
  $sceDelegateProvider.resourceUrlWhitelist(["self", SER + "**"]);

  $stateProvider
  .state("main", {
    astract:true,
    templateUrl: "main/views/layout.html",
    controller: "MainController",
    controllerAs: "main"
  })
  .state("main.home", {
    url: "/",
    views: {
      "content": {
        templateUrl: "main/views/home.html",
        controller: "MainController",
        controllerAs: "main"
       }
    }

  })
  .state("main.register", {
    url: "/register",
    views: {
      "content": {
        templateUrl: "auth/views/register.html",
        controller: "RegisterController",
        controllerAs: "register"
       }
    }
  })
  .state("main.login", {
    url: "/login",
    views: {
      "content": {
        templateUrl: "auth/views/login.html",
        controller: "LoginController",
        controllerAs: "login"
       }
    }
  })
  .state("main.runs", {
    url: "/runs",
    views: {
      "content": {
        templateUrl: "runs/views/runs.html",
        controller: "RunsController",
        controllerAs: "runs"
       }
    }
  })
  .state("main.report", {
    url: "/report",
    views: {
      "content": {
        templateUrl: "runs/views/report.html",
        controller: "ReportController",
        controllerAs: "report"
       }
    }
  });

  $urlRouterProvider.otherwise("/");
});
