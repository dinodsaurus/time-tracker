"use strict";
class MainController{
  constructor($scope, UserService, localStorageService, $state){
    var self = this;

    this.localStorageService = localStorageService;
    this.UserService = UserService;
    this.$state = $state;
    this.user = {};
    var user = localStorageService.get("user");
    if(user){
      this.login(user);
    }

    $scope.$on("userLogin", function() {
      user = localStorageService.get("user");
      self.login(user);
    });
  }
  login(user){
    this.UserService.setHttpDefaults();
    this.user.email = user.email;
  }
  logout(){
    this.user = {};
    this.localStorageService.set("user","");
    this.UserService.setHttpDefaults();
    this.$state.go("main.home");
  }
}
angular.module("easyrun").controller("MainController", MainController);
