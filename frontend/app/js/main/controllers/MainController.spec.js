"use strict";

describe("Main", function(){
  var scope,controller;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller("MainController as main", {
        $scope: scope
      });
    }));

    it("should have default methods defined", function() {
      expect(scope.main.user).toBeDefined();
      expect(scope.main.UserService).toBeDefined();
      expect(scope.main.$state).toBeDefined();
      expect(scope.main.localStorageService).toBeDefined();
    });

    it("should login a user", function() {
      var user = {
        email: "easyrun@gmail.com",
        token: "12345"
      };
      scope.main.login(user);
      expect(scope.main.user.email).toBe("easyrun@gmail.com");
    });

    it("should logout a user", function() {
      scope.main.logout();
      expect(scope.main.user.email).toBeUndefined();
      expect(scope.main.user.token).toBeUndefined();
    });
  });
});
