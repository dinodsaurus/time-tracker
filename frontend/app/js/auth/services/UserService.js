"use strict";
class UserService{
  constructor($http, $q, SER, localStorageService){
    this.$http = $http;
    this.$q = $q;
    this.SER = SER;
    this.localStorageService = localStorageService;
  }
  setHttpDefaults() {
    var user = this.localStorageService.get("user");
    if(user){
      this.$http.defaults.headers.common.Authorization = user.token;
    }
  }
  registerUser(user){
    var q = this.$q.defer();

    this.$http.post(this.SER + "register", user).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }

  loginUser(user){
    var q = this.$q.defer();

    this.$http.post(this.SER + "authenticate", user).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }
}

angular.module("easyrun").service("UserService", UserService);
