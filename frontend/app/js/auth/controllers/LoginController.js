"use strict";
class LoginController{
  constructor($scope, UserService, localStorageService, $state){
    this.localStorageService = localStorageService;
    this.$scope = $scope;
    this.$state = $state;
    this.UserService = UserService;
    this.user = {};
  }
  userLogin(){
    var self = this;
    this.UserService.loginUser(this.user).then( data => {
        self.formError = false;
        self.localStorageService.set("user", data);
        self.$scope.$emit("userLogin");
        self.$state.go("main.runs");
    }, function () {
      self.localStorageService.set("user", "");
      self.formError = true;
    });
  }
}
angular.module("easyrun").controller("LoginController", LoginController);
