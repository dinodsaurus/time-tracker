"use strict";
class RegisterController{
  constructor(UserService, $state, localStorageService, $scope){
    this.UserService = UserService;
    this.user = {};
    this.$state = $state;
    this.localStorageService = localStorageService;
    this.$scope = $scope;
  }
  register(){
    var self = this;
    this.UserService.registerUser(this.user).then( data => {
      self.localStorageService.set("user", data);
      self.$scope.$emit("userLogin");
      self.$state.go("main.runs");
    }, err => {
      self.localStorageService.set("user", "");
      if(err.err.code === 11000){
        self.userExists = true;
      }
    });
  }
}
angular.module("easyrun").controller("RegisterController", RegisterController);
