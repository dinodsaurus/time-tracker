"use strict";

describe("Register", function(){
  var scope,controller, http, ser;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function($rootScope, $controller, $httpBackend, SER) {
      scope = $rootScope.$new();
      controller = $controller("RegisterController as register", {
        $scope: scope
      });
      http = $httpBackend;
      ser = SER;
    }));

    it("should have default methods defined", function() {
      expect(scope.register.user).toBeDefined();
      expect(scope.register.UserService).toBeDefined();
    });

    it("should register a user successfully", function (done) {
      scope.register.user = {
        email: "easyrun@gmail.com",
        password: "easyrun"
      };

      http.expectPOST(ser + "register").respond(200,{email: "easyrun@gmail.com", token: 12345});

      scope.register.register();
      http.flush();

      var ls = scope.register.localStorageService.get("user");
      expect(ls.email).toBe("easyrun@gmail.com");
      expect(ls.token).toBe(12345);
      expect(scope.register.userExists).toBeUndefined();
      done();

    });

    it("should register a user unsuccessfully", function (done) {
      scope.register.user = {
        email: "easyrun@gmail.com",
        password: "easyrun"
      };

      http.expectPOST(ser + "register").respond(400,{err: {code: 11000}});

      scope.register.register();
      http.flush();

      var ls = scope.register.localStorageService.get("user");
      expect(ls.email).toBeUndefined();
      expect(ls.token).toBeUndefined();
      expect(scope.register.userExists).toBeTruthy();
      done();

    });

  });
});
