"use strict";

describe("Login", function(){
  var scope,controller, http, ser;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function($rootScope, $controller, $httpBackend, SER) {
      scope = $rootScope.$new();
      controller = $controller("LoginController as login", {
        $scope: scope
      });
      http = $httpBackend;
      ser = SER;
    }));

    it("should have default methods defined", function() {
      expect(scope.login.user).toBeDefined();
      expect(scope.login.localStorageService).toBeDefined();
      expect(scope.login.UserService).toBeDefined();
      expect(scope.login.userLogin).toBeDefined();
    });

    it("should login a user successfully", function (done) {
      scope.login.user = {
        email: "easyrun@gmail.com",
        password: "easyrun"
      };

      http.expectPOST(ser + "authenticate").respond(200,{email: "easyrun@gmail.com", token: 12345});

      scope.login.userLogin();
      http.flush();

      var ls = scope.login.localStorageService.get("user");
      expect(ls.email).toBe("easyrun@gmail.com");
      expect(ls.token).toBe(12345);
      expect(scope.login.formError).toBeFalsy();
      done();

    });

    it("should login a user unsuccessfully", function (done) {
      scope.login.user = {
        email: "easyrun@gmail.com",
        password: "easyrun"
      };

      http.expectPOST(ser + "authenticate").respond(400,{err: "Bad error!"});
      scope.login.userLogin();
      http.flush();

      var ls = scope.login.localStorageService.get("user");
      expect(ls.email).toBeUndefined();
      expect(ls.token).toBeUndefined();
      expect(scope.login.formError).toBeTruthy();
      done();

    });

  });
});
