"use strict";
/* global moment */
angular.module("easyrun").filter("datefilter", function () {
  return function(items, startDate, endDate){
    var res = [],
        start = "",
        end = "";
    if(startDate){
      start = moment(startDate, "DD.MM.YYYY");

    }
    if(endDate){
      end = moment(endDate, "DD.MM.YYYY");
    }
    if(!start && !end){
      return items;
    }
    items.forEach(item => {
      if(start && end){
        console.log("ima oboje");
        if(moment(item.rundate).isAfter(start) && moment(item.rundate).isBefore(end)){
          res.push(item);
        }
      }else if(start){
        console.log("ima start");
        if(moment(item.rundate).isAfter(start)){
          res.push(item);
        }
      }else if(end){
        console.log("ima end");
        if(moment(item.rundate).isBefore(end)){
          res.push(item);
        }
      }
    });
    return res;
  };
});
