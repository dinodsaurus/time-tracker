"use strict";
angular.module("easyrun")
.directive("addRun", function(){
  return {
    restrict: "A",
    scope: {
      toggleModal: "&",
      addRun: "&",
      updateRun: "&",
      editRun: "="
    },
    templateUrl: "runs/views/addrun.html",
    controller: "AddRunController",
    controllerAs: "addrun"
  };
});
