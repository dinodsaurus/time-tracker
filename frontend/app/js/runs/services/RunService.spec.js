"use strict";

describe("RunService", function(){
  var runServ;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function(RunService) {
      runServ = RunService;
    }));
    it("Should have defined methods", function() {
      expect(runServ.mathRound).toBeDefined();
      expect(runServ.saveRun).toBeDefined();
      expect(runServ.editRun).toBeDefined();
      expect(runServ.getAllRuns).toBeDefined();
      expect(runServ.deleteRun).toBeDefined();
    });
    it("Should round the numbers", function() {
      var res = runServ.mathRound(1.005, 2);
      expect(res).toBe(1.01);
      res = runServ.mathRound(10, 2);
      expect(res).toBe(10);
      res = runServ.mathRound(1.777777777, 2);
      expect(res).toBe(1.78);
      res = runServ.mathRound(9.1, 2);
      expect(res).toBe(9.1);
    });
  });
});
