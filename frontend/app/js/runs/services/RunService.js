"use strict";
class RunService{
  constructor($http, $q, SER, localStorageService){
    this.$http = $http;
    this.$q = $q;
    this.SER = SER;
    this.localStorageService = localStorageService;
  }
  mathRound(num, places){
    return +(Math.round(num + "e+" + places)  + "e-" + places);
  }
  saveRun(run){
    var q = this.$q.defer();

    this.$http.post(this.SER + "runs", run).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }
  editRun(id, run){
    var q = this.$q.defer();

    this.$http.put(this.SER + "runs/" + id, run).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }
  getAllRuns(email){
    var q = this.$q.defer();
    this.$http.get(this.SER + "runs", { params: {email: email}}).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }
  deleteRun(run){
    var q = this.$q.defer();
    this.$http.delete(this.SER + "runs/" + run._id).
      success( data => {
        q.resolve(data);
      }).
      error( err => {
        q.reject(err);
      });

    return q.promise;
  }
}

angular.module("easyrun").service("RunService", RunService);
