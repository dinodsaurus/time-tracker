"use strict";
/* global moment*/
class RunsController{
  constructor(RunService, localStorageService, $state){
    var self = this;
    this.runs = [];
    if(!localStorageService.get("user")){
      $state.go("main.home");
    }
    this.RunService = RunService;

    var user = localStorageService.get("user");
    this.RunService.getAllRuns(user.email).then(data => {
      data.runs.forEach(run => {
        run = self.populateRun(run);
      });
      self.runs = data.runs.slice().reverse();
    });
  }
  showDetail(run){
    run.active = !run.active;
  }
  addRun(run){
    var r = this.populateRun(run);
    this.runs.unshift(r);
  }
  editRun(run){
    this.toggleModal();
    this.runToEdit = run;
  }
  updateRun(id, run){
    var self = this;
    this.runs.forEach( (r, i) => {
      if(r._id === id){
        self.runs[i] = self.populateRun(run);
      }
    });
  }
  deleteRun(run, i){
    this.runs.splice(i, 1);
    this.RunService.deleteRun(run);
  }
  toggleModal(){
    this.runToEdit = false;
    this.showAddNew = !this.showAddNew;
  }
  populateRun(run){
    run.momTime = moment().startOf("day")
        .seconds(run.time)
        .format("H:mm:ss");

    run.average = this.RunService.mathRound((run.distance/run.time) * 3.6, 2);
    return run;
  }
}
angular.module("easyrun").controller("RunsController", RunsController);
