"use strict";
/* global moment*/
class AddRunController{
  constructor(RunService, $scope, localStorageService){
    this.RunService = RunService;
    this.entry = {};
    this.$scope = $scope;
    this.localStorageService = localStorageService;
    if($scope.editRun){
      this.entry = angular.copy($scope.editRun);
      this.entry.date = moment(this.entry.rundate).format("DD.MM.YYYY");
    }
  }
  closeModal(){
    this.$scope.toggleModal();
  }
  editSaveRun(){
    var self = this;
    var run = this.formatRun();
    run._id = this.entry._id;

    this.RunService.editRun(run._id, run).then(function () {
      self.$scope.updateRun({id: run._id, run: run});
      self.$scope.toggleModal();
    });
  }
  saveRun(){
    // if we are edinting a run
    if(this.$scope.editRun){
      this.editSaveRun();
    }else{
      var run = this.formatRun();
      this.RunService.saveRun(run).then( data => {
        this.$scope.addRun({run: data.run});
        this.$scope.toggleModal();
      });
    }
  }
  formatRun(){
    var self = this;
    var date = moment(this.entry.date, "DD.MM.YYYY").format();
    var user = this.localStorageService.get("user");
    var r = {
      email: user.email,
      time: self.entry.time,
      distance: self.entry.distance,
      rundate: date
    };
    return r;
  }
}
angular.module("easyrun").controller("AddRunController", AddRunController);
