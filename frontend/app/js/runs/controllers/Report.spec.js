"use strict";
/* global moment */
describe("ReportController", function(){
  var scope,controller;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller("ReportController as report", {
        $scope: scope
      });
    }));

    it("should have default methods defined", function() {
      expect(scope.report.report).toBeDefined();
      expect(scope.report.sanitizeReport).toBeDefined();
    });

    it("should sanite runs #1", function() {
      var runs = [
        {
          rundate: moment("01.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 100,
        }
      ];
      scope.report.sanitizeReport(runs);

      expect(scope.report.report["01.06.2015"]).toBeDefined();
      expect(scope.report.report["01.06.2015"].start).toBe("01.06");
      expect(scope.report.report["01.06.2015"].end).toBe("07.06");
      expect(scope.report.report["01.06.2015"].distance).toBe(100);
      expect(scope.report.report["01.06.2015"].speed).toBe(3.6);

    });
    
    it("should sanite runs #2", function() {
      var runs = [
        {
          rundate: moment("01.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 100,
        },
        {
          rundate: moment("03.06.2015", "DD.MM.YYYY"),
          time: 200,
          distance: 200,
        }
      ];

      scope.report.sanitizeReport(runs);

      expect(scope.report.report["01.06.2015"]).toBeDefined();
      expect(scope.report.report["01.06.2015"].start).toBe("01.06");
      expect(scope.report.report["01.06.2015"].end).toBe("07.06");
      expect(scope.report.report["01.06.2015"].distance).toBe(150);
      expect(scope.report.report["01.06.2015"].speed).toBe(3.6);

    });
    it("should sanite runs #3", function() {
      var runs = [
        {
          rundate: moment("01.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 100,
        },
        {
          rundate: moment("03.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 200,
        },
        {
          rundate: moment("03.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 300,
        }
      ];
      scope.report.sanitizeReport(runs);

      expect(scope.report.report["01.06.2015"]).toBeDefined();
      expect(scope.report.report["01.06.2015"].start).toBe("01.06");
      expect(scope.report.report["01.06.2015"].end).toBe("07.06");
      expect(scope.report.report["01.06.2015"].distance).toBe(200);
      expect(scope.report.report["01.06.2015"].speed).toBe(7.2);
    });

    it("should sanite runs #4", function() {
      var runs = [
        {
          rundate: moment("01.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 100,
        },
        {
          rundate: moment("03.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 200,
        },
        {
          rundate: moment("03.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 300,
        },
        {
          rundate: moment("08.06.2015", "DD.MM.YYYY"),
          time: 100,
          distance: 300,
        }
      ];
      scope.report.sanitizeReport(runs);

      expect(scope.report.report["01.06.2015"]).toBeDefined();
      expect(scope.report.report["08.06.2015"]).toBeDefined();
      expect(scope.report.report["01.06.2015"].start).toBe("01.06");
      expect(scope.report.report["01.06.2015"].end).toBe("07.06");
      expect(scope.report.report["01.06.2015"].distance).toBe(200);
      expect(scope.report.report["01.06.2015"].speed).toBe(7.2);

      expect(scope.report.report["08.06.2015"].speed).toBe(10.8);
      expect(scope.report.report["08.06.2015"].distance).toBe(300);

    });

  });
});
