"use strict";
describe("RunsController", function(){
  var scope,controller;

  beforeEach(module("easyrun"));
  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    controller = $controller("RunsController as runs", {
      $scope: scope
    });
  }));
  describe("methods testing", function () {

    it("should have default methods defined", function() {
      expect(scope.runs.RunService).toBeDefined();
      expect(scope.runs.showDetail).toBeDefined();
      expect(scope.runs.populateRun).toBeDefined();
    });

    it("should toggle detail view of a run", function () {
      var run = {};
      scope.runs.showDetail(run);
      expect(run.active).toBeTruthy();
    });

    it("should add a new run", function () {
      expect(scope.runs.runs.length).toBe(0);
      var run = {
        time: 3600,
        distance: 1000
      };
      scope.runs.addRun(run);
      scope.runs.addRun(run);
      expect(scope.runs.runs.length).toBe(2);
    });

    it("should delete a run", function () {
      var run = {
        time: 3600,
        distance: 1000
      };
      scope.runs.addRun(run);
      expect(scope.runs.runs.length).toBe(1);
      scope.runs.deleteRun({}, 0);
      expect(scope.runs.runs.length).toBe(0);
    });

    it("should toggle modal", function () {
      scope.runs.toggleModal();
      expect(scope.runs.runToEdit).toBeFalsy();
      expect(scope.runs.showAddNew).toBeTruthy();
      scope.runs.toggleModal();
      expect(scope.runs.runToEdit).toBeFalsy();
      expect(scope.runs.showAddNew).toBeFalsy();

    });

    it("should populate a run", function () {
      var run = {
        time: 3600,
        distance: 1000
      };

      var res = scope.runs.populateRun(run);
      expect(res.momTime).toBe("1:00:00");
      expect(res.average).toBe(1);

    });
  });
});
