"use strict";
/* global moment */
describe("AddRunController", function(){
  var scope,controller;

  beforeEach(module("easyrun"));

  describe("methods testing", function () {
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller("AddRunController as addrun", {
        $scope: scope
      });
    }));

    it("should have default methods defined", function() {
      expect(scope.addrun.formatRun).toBeDefined();
      expect(scope.addrun.closeModal).toBeDefined();
      expect(scope.addrun.saveRun).toBeDefined();
    });

    it("should format a run", function () {
      scope.addrun.localStorageService.set("user", {email: "easyrun@gmail.com", token: 12345});

      scope.addrun.entry.date = "01.06.2015";
      scope.addrun.entry.distance = 255;
      scope.addrun.entry.time = 355;
      var run = scope.addrun.formatRun();
      expect(run.email).toBe("easyrun@gmail.com");
      expect(run.time).toBe(355);
      expect(run.distance).toBe(255);
      
      var date = moment("01.06.2015" , "DD.MM.YYYY").isSame(run.rundate);
      expect(date).toBeTruthy();
    });

  });
});
