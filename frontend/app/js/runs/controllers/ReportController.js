"use strict";
/* global moment*/
class ReportController{
  constructor(RunService, localStorageService, $state){
    var self = this;
    var user = localStorageService.get("user");
    if(!localStorageService.get("user")){
      $state.go("main.home");
    }
    this.report = {};

    this.RunService = RunService;
    this.RunService.getAllRuns(user.email).then(data => {
      self.sanitizeReport(data.runs);
    });
  }
  sanitizeReport(runs){
    var self = this;
    if(!runs.length){
      self.report = false;
    }
    runs.forEach( run => {
      var start = moment(run.rundate).startOf("isoWeek");
      var end = moment(run.rundate).endOf("isoWeek");
      var s = start.format("DD.MM.YYYY");
      if(self.report[s]){
        var runs = self.report[s].el,
            distance = 0,
            time = 0;

        runs.push(run);
        runs.forEach( run => {
          distance +=  run.distance;
          time += run.time;
        });

        var speed = self.RunService.mathRound((distance/time) * 3.6, 2);
        self.report[s].distance = distance / runs.length;
        self.report[s].speed =  speed;

      }else{
        self.report[s] = {
          start: start.format("DD.MM"),
          end: end.format("DD.MM"),
          distance: run.distance,
          speed: self.RunService.mathRound((run.distance/run.time) * 3.6, 2),
          el: [run]
        };
      }
    });
  }
}
angular.module("easyrun").controller("ReportController", ReportController);
