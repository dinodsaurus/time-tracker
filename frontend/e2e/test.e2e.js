"use strict";
describe("Eeasy run", function() {
  var email;

  browser.get("http://localhost:9000/");
  browser.waitForAngular();

  it("should have a title", function() {
    expect(browser.getTitle()).toEqual("Easy run - Track your runs");
  });
  it("should not go to runs", function() {
    browser.get("http://localhost:9000/#/runs");
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/");
  });
  it("should got to register", function() {
    element(by.css(".reg")).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/register");
  });
  it("should input register credentials", function() {
    var rand = Math.floor((Math.random() * 50000) + 1);
    var $email = element(by.css("#email"));
    var $password = element(by.css("#password"));
    email = "easyrun" + rand;

    $email.sendKeys("easyrun" + rand);
    element(by.css("#subitReg")).click();
    expect($email.getAttribute("class")).toMatch("ng-invalid");
    expect($password.getAttribute("class")).toMatch("ng-invalid");
    $email.sendKeys("@gmail.com");
    expect($email.getAttribute("class")).toMatch("ng-valid");
    expect($password.getAttribute("class")).toMatch("ng-invalid");
    element(by.css("#subitReg")).click();
    $password.sendKeys("easyrun");
    element(by.css("#subitReg")).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/runs");
  });
  it("should not have runs displayed", function() {
    var runs = element.all(by.repeater("run in runs.runs"));
    expect(runs.count()).toEqual(0);
  });
  it("should logout", function() {
    element(by.css(".logout")).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/");
  });
  it("should login", function() {
    element(by.css(".login")).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/login");
  });
  it("should input wrong login credentials", function() {
    var $email = element(by.css("#email"));
    var $password = element(by.css("#password"));
    var $err = element(by.css(".errMsg"));

    $email.sendKeys("easyrun");
    element(by.css("#subitReg")).click();
    expect($email.getAttribute("class")).toMatch("ng-invalid");
    expect($password.getAttribute("class")).toMatch("ng-invalid");
    $email.sendKeys("@gmail.com");
    expect($email.getAttribute("class")).toMatch("ng-valid");
    expect($password.getAttribute("class")).toMatch("ng-invalid");
    $password.sendKeys("easyrun");
    expect($password.getAttribute("class")).toMatch("ng-valid");
    element(by.css("#subitReg")).click();
    expect($err.isDisplayed()).toBeTruthy();
  });
  it("should input correct login credentials", function() {
    var $email = element(by.css("#email"));
    $email.clear();
    $email.sendKeys(email + "@gmail.com");
    element(by.css("#subitReg")).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/runs");
  });
  it("should show add run modal", function() {
    var $add = element(by.css(".add"));
    var $popup = element(by.css(".popup"));
    $add.click();
    expect($popup.isDisplayed()).toBeTruthy();
  });
  it("should add a new run", function() {
    var $date = element(by.css("#date"));
    var $distance = element(by.css("#distance"));
    var $time = element(by.css("#time"));
    var $popup = element(by.css(".popup"));

    $distance.sendKeys("1000");
    $time.sendKeys("234");
    $date.sendKeys("18.06.2015");
    $date.sendKeys(protractor.Key.ENTER);
    var runs = element.all(by.repeater("run in runs.runs"));
    expect($popup.isPresent()).toBeFalsy();
    expect(runs.count()).toEqual(1);
  });
  it("should open edit a run", function () {
    var $popup = element(by.css(".popup"));
    var runs = element.all(by.repeater("run in runs.runs"));
    runs.get(0).element(by.css(".click")).click();
    runs.get(0).element(by.css(".edit")).click();
    expect($popup.isDisplayed()).toBeTruthy();
  });

  it("should edit a run", function () {
    var $popup = element(by.css(".popup"));
    var $distance = element(by.css("#distance"));
    var runs = element.all(by.repeater("run in runs.runs"));

    $distance.sendKeys("234");
    $distance.sendKeys(protractor.Key.ENTER);
    expect($popup.isPresent()).toBeFalsy();
    expect(runs.get(0).element(by.css(".distance")).getText()).toEqual("1000234 m");
  });

  it("should show add run modal again", function() {
    var $add = element(by.css(".add"));
    var $popup = element(by.css(".popup"));
    $add.click();
    expect($popup.isDisplayed()).toBeTruthy();
  });

  it("should add a new run again", function() {
    var $date = element(by.css("#date"));
    var $distance = element(by.css("#distance"));
    var $time = element(by.css("#time"));
    var $popup = element(by.css(".popup"));

    $distance.sendKeys("200");
    $time.sendKeys("300");
    $date.sendKeys("03.06.2015");
    $date.sendKeys(protractor.Key.ENTER);
    var runs = element.all(by.repeater("run in runs.runs"));
    expect($popup.isPresent()).toBeFalsy();
    expect(runs.count()).toEqual(2);
  });

  it("should go to report and check it", function() {
    var $rep = element(by.css(".goToReport"));
    $rep.click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/report");
  });

  it("should check number of reports", function () {
    var $runs = element(by.css(".goToRuns"));
    var reports = element.all(by.css(".report"));
      expect(reports.count()).toEqual(2);
    $runs.click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:9000/#/runs");
  });

  it("should delete a run", function () {
    var runs = element.all(by.repeater("run in runs.runs"));
    runs.get(0).element(by.css(".click")).click();
    runs.get(0).element(by.css(".delete")).click();
    expect(runs.count()).toEqual(1);
  });

});
