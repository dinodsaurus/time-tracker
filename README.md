# Easy run
## [Dino Trojak](http://dinodsaur.us) - Test project

Easy run is made to easily track or your jogging stats and habits.


### Project consists of:
* Express.js
* MongoDB
* Angular.js
* Gulp
* Jasmine/Karma
* Protractor

The project is separated into 2 folders: frontend and backend. They are independent of eachother.  [CORS](https://www.npmjs.com/package/cors) is enabled but the frontend could be easily packed into backends public folder.


### How to start project
* Go to `backend` folder and run `npm install`
* You need to have `MongoDB` and run it with `mongod`
* To start the server run `npm start` inside the `backend` folder
* Open new terminal window
* Go to `frontend` folder and run `npm install && bower install`
* To start the frontend server run `npm start` inside `frondend` folder


### Project testing

* Go to `frontend` and run the unit tests with `gulp test`
* To run E2E tests first spin up the backend and frontend server with `npm start` in proper folder
* Start selenium webdriver with `webdriver-manager start`
* Run the E2E tests with `gulp e2e`

Unit/E2E tests are done only on the frontend!

### Frontend tasks

* `gulp` - to make a production build inside `.dist`
* `gulp serve` - run browser sync server
* `gulp test` - run unit tests using karma/jasmine
* `webdriver-manager start` - starts selenium webdriver
* `gulp e2e` - run e2e tests with protractor
* `gulp scripts` - transform es6 to es5
* `gulp styles` - transform scss to css and run autoprefixer on it

For more tasks please refer to `gulpfile.js`
