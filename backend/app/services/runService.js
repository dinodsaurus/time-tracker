"use strict";
var Run = require("../models/run"),
    q = require("q");

var RunService = {};

RunService.getRuns = function (email) {
  var def = q.defer();
  Run.find({email:email}, function(err, runs) {
    if (err){
      throw err;
    }
    def.resolve({ runs: runs});
  });
  return def.promise;
};

RunService.saveRun = function (run) {
  var def = q.defer();
  console.log(run);
  var newRun = new Run(run);

  newRun.save(function(err, run) {
    if (err){
      def.reject(err);
    }
    def.resolve({ run: run});
  });

  return def.promise;
};

RunService.getRun = function (id) {
  var def = q.defer();
  Run.find({id: id}, function(err, run) {
    if (err){
      throw err;
    }
    if(run){
      def.resolve({ run: run});
    }else{
      def.reject({ err: "No run with that id"});
    }
  });
  return def.promise;
};

RunService.editRun = function (id, run) {
  var def = q.defer();
  console.log(id);
  Run.update({_id: id}, run, function (err, data) {
    if(err){
      def.reject({err: err});
    }else{
      def.resolve({run: data});
    }
  });
  return def.promise;
};

RunService.deleteRun = function (id) {
  var def = q.defer();
  Run.remove({_id: id}, function(err, run) {
    if(err){
      def.reject({err: err});
    }else{
      def.resolve({run: run});
    }
  });
  return def.promise;
};

module.exports = RunService;
