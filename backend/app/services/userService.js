"use strict";
var User = require("../models/user"),
    config = require("../../config/config"),
    jwt    = require("jsonwebtoken"),
    q = require("q"),
    bcrypt = require("bcrypt");

var UserService = {};

UserService.emailRegex = function(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

UserService.comparePass = function (oldPas, newPas) {
  var def = q.defer();
  bcrypt.compare(oldPas, newPas, function(err, res) {
    if(err){
      throw err;
    }
    if(res){
      def.resolve();
    }else{
      def.reject();
    }
  });
  return def.promise;
};

UserService.saveUser = function (email, pswd) {
  var def = q.defer();
  var newUser = new User({
    email: email,
    password: pswd
  });

  newUser.save(function(err) {
    if (err){
      def.reject(err);
    }
    def.resolve();
  });
  return def.promise;
};

UserService.registerUser = function (email, password) {
  var def = q.defer();
  if(!UserService.emailRegex(email)){
    def.reject({success: false, err: "Not valid email"});
  }else{
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(password, salt, function(err, hash) {
          if(err){
            throw err;
          }
          UserService.saveUser(email, hash).then(function () {
            var token = jwt.sign(email, config.secrete , {
              expiresInMinutes: 1440 // expires in 24 hours
            });
            def.resolve({success:true, token: token, email: email});
          }, function (err) {
            def.reject({success: false, err: err});
          });
      });
    });
  }
  return def.promise;
};

UserService.loginUser = function (email, password) {
  var def = q.defer();
  if(!UserService.emailRegex(email)){
    def.reject({success: false, err: "Not valid email"});
  }else{
    User.findOne({
      email: email
    }, function(err, user) {
      if (err){
        throw err;
      }
      if (!user) {
        def.reject({ success: false, message: "Authentication failed. User not found." });
      } else if (user) {
        UserService.comparePass(password, user.password).then(function () {
          var token = jwt.sign(user.email, config.secrete , {
            expiresInMinutes: 1440 // expires in 24 hours
          });
          def.resolve({success:true, token: token, email: user.email});
        }, function () {
          def.reject({ success: false, message: "Authentication failed. Wrong password." });
        });
      }
    });
  }
  return def.promise;
};

module.exports = UserService;
