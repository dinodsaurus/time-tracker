"use strict";
var express = require("express"),
    router = express.Router(),
    UserService = require("../services/userService.js");

router.post("/authenticate", function(req, res) {
  if(!req.body.email || !req.body.password){
    res.json(400, {err: "No email or password."});
  }
  UserService.loginUser(req.body.email, req.body.password).then(function(data){
    res.json(data);
  }, function (err) {
    res.json(400, err);
  });
});

router.post("/register", function(req, res) {
  if(!req.body.email || !req.body.password){
    res.json({err: "No email or password."});
  }
  UserService.registerUser(req.body.email, req.body.password).then(function(data){
    res.json(data);
  }, function (err) {
    res.json(400, err);
  });
});

module.exports = function (app) {
  app.use("/api", router);
};
