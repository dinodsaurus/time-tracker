"use strict";
var express = require("express"),
    router = express.Router(),
    jwt    = require("jsonwebtoken"),
    config = require("../../config/config"),
    RunService = require("../services/runService.js");

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers.authorization;

  if (token) {
    jwt.verify(token, config.secrete, function(err, decoded) {
      if (err) {
        return res.json(403, { message: "Failed to authenticate token." });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).send({
        message: "No token provided."
    });
  }
});

router.route("/")
  .post(function(req, res) {
    RunService.saveRun(req.body).then(function (data) {
      res.json(data);
    }, function (err) {
      res.json(403, err);
    });

  })
  .get(function(req, res) {
    RunService.getRuns(req.query.email).then(function (data) {
      res.json(data);
    }, function (err) {
      res.json(403, err);
    });
  });


router.route("/:id")
  .get(function(req, res) {
    RunService.getRun(req.params.id).then(function (data) {
      res.json(data);
    }, function (err) {
      res.json(403, err);
    });
  })
  .put(function (req, res) {
    RunService.editRun(req.params.id, req.body).then(function (data) {
      res.json(data);
    }, function (err) {
      res.json(403, err);
    });
  })
  .delete(function (req, res) {
    RunService.deleteRun(req.params.id).then(function (data) {
      res.json(data);
    }, function (err) {
      res.json(403, err);
    });
  });



module.exports = function (app) {
  app.use("/api/runs", router);
};
