"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var RunSchema = new Schema({
    rundate: {
      type: Date,
      required: true
    },
    distance: {
      type: Number,
      required: true
    },
    time: {
      type: Number,
      required: true
    },
    email: {
      type: String,
      required: true
    }
});

RunSchema.virtual("date").get(function(){
  return this._id.getTimestamp();
});

module.exports = mongoose.model("Run", RunSchema);
