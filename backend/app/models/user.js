"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: {
      type: String,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true
    }
});

UserSchema.virtual("date").get(function(){
  return this._id.getTimestamp();
});
module.exports = mongoose.model("User", UserSchema);
