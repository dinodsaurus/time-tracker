var path = require("path"),
    rootPath = path.normalize(__dirname + "/.."),
    env = process.env.NODE_ENV || "development";

var config = {
  development: {
    root: rootPath,
    app: {
      name: "EasyRun",
    },
    secrete: "EasyRunIsAwesome!",
    port: 3000,
    db: "mongodb://localhost/easyrun-development"
  },

  test: {
    root: rootPath,
    app: {
      name: "EasyRun"
    },
    port: 3000,
    db: "mongodb://localhost/easyrun-test"
  },

  production: {
    root: rootPath,
    app: {
      name: "EasyRun"
    },
    port: 3000,
    db: "mongodb://localhost/easyrun-production"
  }
};

module.exports = config[env];
